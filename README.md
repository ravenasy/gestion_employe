# gestion_employe


## installation 

sur le racine de projet , lancer la commande suivante:

```bash
composer install
```



## configuration base de donnée

verifier la connection vers vvotre serveur base de donne dans le fichier .env.local

lancer les commandes suivantes

```bash
php bin/console d:d:c
```

```bash
php bin/console d:s:u --force
```


## lancement


```bash
php -S localhost:8888 -t public
```



## utlisation

ouvrir sur le navigateur l'url loaclhost:8888/api

vous pouver utilister des outils comme postman pour verifier la fonctionnement de cet API.
